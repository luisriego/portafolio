import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InfoService } from '../../../services/info.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {

  constructor(public _is:InfoService, private router:Router) { }

  buscarProducto(termino:string){
    this.router.navigate(['buscar', termino]);
  }


}
