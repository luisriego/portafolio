import { Component } from '@angular/core';

// Services
import { InfoService } from '../../../services/info.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent {

  anho:Date = new Date();

  constructor( public _is:InfoService ) { }

}
