import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductosService } from '../../services/productos.service';

@Component({
  selector: 'app-portafolio-item',
  templateUrl: './portafolio-item.component.html',
  styleUrls: ['./portafolio-item.component.css']
})
export class PortafolioItemComponent implements OnInit {

  producto:any = undefined;
  cod:string = "";
  cargando:boolean = true;

  constructor(private route:ActivatedRoute, public _ps:ProductosService) {
    route.params.subscribe( parametros=>{

      _ps.cargarProducto( parametros['id'] )
         .subscribe( res => {
           this.producto = res.json();
           this.cod = parametros['id'];
           this.cargando = false;
          //  console.log( this.producto, this.cod );
         })
    } )
  }

  ngOnInit() {
  }

}
