import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ProductosService } from '../../services/productos.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html'
})
export class BuscarComponent {

  texto:string = "";

  constructor(private route:ActivatedRoute, public _ps:ProductosService) {
    route.params.subscribe( parametros =>{
      // console.log(parametros['termino']);
      this.texto = parametros['termino'];
      _ps.filtrarProductos(parametros['termino']);
    });
  }


}
