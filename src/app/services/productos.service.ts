import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ProductosService {

  productos:any[] = [];
  productos_filtrados:any[] = [];
  cargando:boolean;

  constructor(private http:Http) {
    this.cargarProductos();
  }

  public cargarProductos(){
    this.cargando = true;

    let promesa = new Promise( (resolve, reject)=>{
      this.http.get('https://portafolio-2b138.firebaseio.com/productos_idx.json')
                .subscribe( data=>{
                  // console.log(data.json());
                  this.cargando = false;
                  this.productos = data.json();
                  resolve();
                });
    });
    return promesa;

  }

  public filtrarProductos(termino:string){
    if(this.productos.length === 0){
      this.cargarProductos().then( ()=>{
        // console.log("termino la carga!");
        this.filtro(termino);
      })
    }else{
      this.filtro(termino);
    }


  }

  private filtro(termino:string){
    this.productos_filtrados = [];

    termino = termino.toLowerCase();

    this.productos.forEach( prod =>{
      if( prod.categoria.indexOf(termino) >=0 || prod.titulo.toLowerCase().indexOf(termino) >=0 ){
        this.productos_filtrados.push(prod);
      }
      // console.log(prod, "=>", this.productos_filtrados);
    });
  }

  public cargarProducto( cod:string ){

    return this.http.get(`https://portafolio-2b138.firebaseio.com/productos/${ cod }.json`);

  }

}
